﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public ComponentSystem[] minigames;
    public Player[] players;

    public UnityEngine.UI.Text pressAnyKeyToStartText;

    public static GameManager instance = null;

    public int GameIndex { get { return 0; } }

    bool isWaitingForInputToStart;
    public DinoScript dinoScript;

    void Awake()
    {

        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        isWaitingForInputToStart = true;
        pressAnyKeyToStartText.gameObject.SetActive(true);
        Time.timeScale = 0;
        if (dinoScript)
            dinoScript.enabled = false;

    }

    void StartGame()
    {

        Time.timeScale = 1;
        if (dinoScript)
            dinoScript.enabled = true;
        players = FindObjectsOfType<Player>();
        minigames[GameIndex].OnStart();

    }

    private void Update()
    {

        if (isWaitingForInputToStart)
        {
            if (Input.anyKeyDown)
            {
                isWaitingForInputToStart = false;
                pressAnyKeyToStartText.gameObject.SetActive(false);
                StartGame();
            }
            return;
        }

        var minigame = minigames[GameIndex];

        foreach (var player in players)
            minigame.OnUpdate(player);
        minigame.OnUpdate(players);
        if (minigame.isDone)
            ChangeLevel(GameIndex);

    }

    public void ChangeLevel(int l)
    {
        SceneManager.LoadScene(l, LoadSceneMode.Single);
    }

    public void Exit()
    {
        Application.Quit();
    }

}

public abstract class ComponentSystem : ScriptableObject
{

    public virtual void OnStart() { }
    public virtual void OnUpdate(Player player) { }
    public virtual void OnUpdate(Player[] players) { }
    public virtual void OnEnd() { }

    [HideInInspector] public bool isDone;

}
