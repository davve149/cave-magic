﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
public class SingleSoundSystem : MonoBehaviour
{

    public AudioScriptableObject[] sounds;
    AudioSource audioSource;
    public UnityEvent OnSoundPlay;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        foreach (var s in sounds)
        {
            audioSource.PlayOneShot(s.AudioClip);
        }

    }
    /// <summary>
    /// Create a AudioScriptableObject, use that when playing a sound with this component
    /// </summary>
    public void PlayEventSound(AudioScriptableObject sound)
    {
        audioSource.volume = sound.Volume;
        audioSource.clip = sound.AudioClip;
        audioSource.outputAudioMixerGroup = sound.AudioMixerGroup;
        audioSource.loop = sound.Loop;
        audioSource.Play();
        OnSoundPlay.Invoke();
    }


}
