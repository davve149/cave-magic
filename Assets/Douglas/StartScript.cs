﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScript : MonoBehaviour {
    public GameObject explosion;
	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(ButtonClick);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void ButtonClick()
    {
        explosion.SetActive(true);
    }
}
