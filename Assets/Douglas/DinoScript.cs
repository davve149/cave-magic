﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DinoScript : MonoBehaviour {
    public float speed = 0.1f;
    // Use this for initialization
    public UnityEvent OnKill;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(new Vector3(speed,0,0));
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
            OnKill.Invoke();
        }
    }
}
