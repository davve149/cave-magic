﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireballVelocity : MonoBehaviour 
{
    public Vector2 Target;
    public float projectileSpeed;

    public Player me;

	void Start () 
	{
		
	}

	void Update () 
	{
		Vector2 dir=Target-(Vector2)transform.position;
		GetComponent<Rigidbody2D>().velocity=dir.normalized*projectileSpeed*Time.deltaTime;
	}
}
