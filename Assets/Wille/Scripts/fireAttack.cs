﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireAttack : MonoBehaviour
{
public Vector2 Target;
public GameObject fireballPrefab;
private Camera cam;
private Rigidbody2D rb;
public float fireballSpeed;
	void Start()
	{
		cam=Camera.main;

	}
	void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			Target=cam.ScreenToWorldPoint(Input.mousePosition);
			GameObject obj=Instantiate(fireballPrefab,transform.position,Quaternion.identity);
			fireballVelocity Fireball=obj.AddComponent<fireballVelocity>();
			Fireball.Target=Target;
			Fireball.projectileSpeed=fireballSpeed;
            Fireball.me = gameObject.GetComponent<Player>();
		}
	}
}