﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Games/Battle")]
public class BattleGame : ComponentSystem
{

    LineRenderer line;

    public override void OnStart()
    {
        var obj = new GameObject("Line");
        line = obj.AddComponent<LineRenderer>();
    }

    public override void OnUpdate(Player player)
    {
        if (player.input.jump)
            player.clicks += 1;
    }

    public override void OnUpdate(Player[] players)
    {

        line.SetPosition(0, players[0].transform.position);
        line.SetPosition(1, (players[0].transform.position - players[1].transform.position));
        line.SetPosition(2, players[1].transform.position);

    }

}
