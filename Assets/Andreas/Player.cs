﻿using UnityEngine;

public class Player : MonoBehaviour
{

    public InputModule input;

    [HideInInspector] public new Rigidbody2D rigidbody;
    [HideInInspector] public bool isJumping;

    [HideInInspector] public float clicks; 

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

}

[System.Serializable]
public struct InputModule
{
    public AxisInput move;
    public InputItem jump;
}

interface IInputItem<T>
{
    T GetValue();
}

[System.Serializable]
public struct InputItem : IInputItem<bool>
{

    public KeyCode button;

    public bool GetValue()
    {
        return Input.GetKey(button);
    }

    public static implicit operator bool(InputItem item)
    {
        return item.GetValue();
    }

}

[System.Serializable]
public struct AxisInput : IInputItem<float>
{

    public KeyCode negativeButton;
    public KeyCode positiveButton;

    public float GetValue()
    {
        return 0 + (Input.GetKey(positiveButton) ? 1 : 0) - (Input.GetKey(negativeButton) ? 1 : 0);
    }

    public static implicit operator float(AxisInput item)
    {
        return item.GetValue();
    }

}
