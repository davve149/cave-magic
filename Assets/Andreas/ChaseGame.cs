﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Games/Chase")]
public class ChaseGame : ComponentSystem
{

    public float moveSpeed;
    public float jumpHeight;
    public float groundedDistanceCheck;

    public override void OnUpdate(Player player)
    {
        Move(player);
        Jump(player);
    }

    void Move(Player player)
    {
        player.rigidbody.position += Vector2.right * player.input.move * moveSpeed * Time.deltaTime;
    }

    void Jump(Player player)
    {
        if (player.input.jump && IsGrounded(player) && !player.isJumping)
        {
            player.isJumping = true;
            player.rigidbody.AddForce(Vector2.up * jumpHeight);
        }
        else if (!player.input.jump)
            player.isJumping = false;
    }

    bool IsGrounded(Player player)
    {
        var hit = Physics2D.BoxCast(player.rigidbody.transform.position, new Vector2(1, 0.01f), 0, Vector2.down, groundedDistanceCheck);
        return hit.collider;
    }

}
