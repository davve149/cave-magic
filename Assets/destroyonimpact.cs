﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyonimpact : MonoBehaviour {


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }

    public Collider2D[] myCollidersToAdd;
    public Collider2D targetCollider;


    // Use this for initialization
    void Start()
    {
        myCollidersToAdd = GetComponents<Collider2D>();

        foreach (var col in myCollidersToAdd)
        {
            Physics2D.IgnoreCollision(GetComponent<fireballVelocity>().me.GetComponent<Collider2D>(), col);
        }
    }

}
